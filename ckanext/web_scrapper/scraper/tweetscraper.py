from ckanext.web_scrapper.plugin import Web_ScrapperPlugin
from ckanext.web_scrapper.plugin import ContentFetchError
from ckan.lib.base import c
from ckan.logic import get_action
from ckan import model
import json
import requests
import ckan.plugins as p


TWEETS_DATE = 'http://srv.dunavnet.eu/ModuleWeliveSocialAPI/api/twitter/' \
              'GetTweetsBySinceDate'

TWEETS = 'http://srv.dunavnet.eu/ModuleWeliveSocialAPI/api/twitter/GetTweets'


class TweetScraper(Web_ScrapperPlugin):

    def info(self):
        return {'name': 'tweet-scraper',
                'title': p.toolkit._('Tweet Scraper'),
                'description': p.toolkit._('''A harvester for integrating the tweet
                                scraper''')}

    def validate_config(self, config):
        try:
            json_config = json.loads(config)
        except Exception as e:
            raise e

        if not (('sinceDate' in json_config and
                'searchQuery' in json_config) or
           ('lat' in json_config and 'long' in json_config and
           'radius' in json_config)):
                raise Exception("At least sinceDate and searchQuery or lat,"
                                " long and radius are required")
        if 'scrapped_dataset_id' not in json_config:
            raise Exception("scrapped_dataset_id not specified")
        else:
            dataset_id = json_config['scrapped_dataset_id']
            context = {'model': model, 'user': c.user}
            try:
                dataset = get_action('package_show')(context, {'id':
                                                               dataset_id})
            except:
                dataset = []
                pass
            if 'type' in dataset:
                if dataset['type'] != 'dataset':
                    raise Exception("%s can't be used as topic_dataset_id"
                                    % dataset_id)
        if 'language' not in json_config:
            raise Exception("language not specified")

        self.config = json_config
        return config

    def fetch_stage(self, harvest_object):
        self.config = json.loads(harvest_object.job.source.config)

        try:
            content = ''
            if 'sinceDate' in self.config and 'searchQuery' in self.config:
                params = {'searchQuery': self.config.get('searchQuery'),
                          'sinceDate': self.config.get('sinceDate')}
                if 'maxTweets' in self.config:
                    params['maxTweets'] = self.config.get('maxTweets')
                content = requests.get(TWEETS_DATE,
                                       verify=False,
                                       params=params).content
            elif ('lat' in self.config and 'long' in self.config and
                  'radius' in self.config):
                params = {'latitude': self.config.get('lat'),
                          'longitude': self.config.get('long'),
                          'radius': self.config.get('radius')}
                if 'maxTweets' in self.config:
                    params['maxTweets'] = self.config.get('maxTweets')
                if 'searchQuery' in self.config:
                    params['searchQuery'] = self.config.get('searchQuery')
                content = requests.get(TWEETS,
                                       verify=False,
                                       params=params).content
        except ContentFetchError, e:
            self._save_object_error('Unable to get content for package: %s: %r'
                                    % (harvest_object.job.url, e),
                                    harvest_object)
            return False

        harvest_object.content = content
        harvest_object.save()

        return True
