from ckanext.web_scrapper.plugin import Web_ScrapperPlugin
from ckanext.web_scrapper.plugin import ContentFetchError
from ckan.lib.base import c
from ckan.logic import get_action
from ckan import model
import json
import requests
import ckan.plugins as p


SCRAPER_URL = 'http://srv.dunavnet.eu/ModuleWeliveSocialAPI/api/scraper/' \
              'GetScrapingForUrlRegex'


class RegexScraper(Web_ScrapperPlugin):

    def info(self):
        return {'name': 'regex-scraper',
                'title': p.toolkit._('Regex based Web Scraper'),
                'description': p.toolkit._('''A harvester for integrating the regex based
                                scraping service''')}

    def validate_config(self, config):
        try:
            json_config = json.loads(config)
        except Exception as e:
            raise e
        if 'regex' not in json_config:
            raise Exception("regex not specified")
        if 'scrapped_dataset_id' not in json_config:
            raise Exception("scrapped_dataset_id not specified")
        else:
            dataset_id = json_config['scrapped_dataset_id']
            context = {'model': model, 'user': c.user}
            try:
                dataset = get_action('package_show')(context, {'id':
                                                               dataset_id})
            except:
                dataset = []
                pass
            if 'type' in dataset:
                if dataset['type'] != 'dataset':
                    raise Exception("%s can't be used as topic_dataset_id"
                                    % dataset_id)
        if 'language' not in json_config:
            raise Exception("language not specified")

        self.config = json_config
        return config

    def fetch_stage(self, harvest_object):
        self.config = json.loads(harvest_object.job.source.config)

        url = harvest_object.job.source.url
        try:
            params = {'url': url, 'regex': self.config.get('regex')}
            content = requests.get(SCRAPER_URL,
                                   verify=False,
                                   params=params).content
        except ContentFetchError, e:
            self._save_object_error('Unable to get content for package: %s: %r'
                                    % (url, e), harvest_object)
            return False

        harvest_object.content = content
        harvest_object.save()

        return True
