from ckanext.harvest.interfaces import IHarvester
from ckanext.harvest.model import HarvestObject
from ckan.lib.base import c
from ckan.logic import get_action
from ckan import model
from tempfile import NamedTemporaryFile
from ckan.logic.action import create
import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit
import json
import cgi


class Web_ScrapperPlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IConfigurer)
    plugins.implements(IHarvester)

    config = None

    # IConfigurer

    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'web_scrapper')

    # IHarvester

    def gather_stage(self, harvest_job):
        self.config = json.loads(harvest_job.source.config)

        package_id = self.config.get('scrapped_dataset_id')
        obj = HarvestObject(guid=package_id, job=harvest_job)
        obj.save()

        return [obj.id]

    def import_stage(self, harvest_object):
        self.config = json.loads(harvest_object.job.source.config)

        context = {'model': model, 'user': c.user}
        dataset_id = self.config.get('scrapped_dataset_id')
        dataset = {}
        try:
            dataset = get_action('package_show')(context, {'id': dataset_id})
        except:
            source_name = 'Social Network'
            harvester_type = harvest_object.job.source.type
            if harvester_type in ['regex-scraper', 'xpath-scraper']:
                source_name = 'Web Scraper'
            source_dataset = model.Package.get(harvest_object.job.source.id)
            dataset = create.package_create(
                context, {'name': dataset_id,
                          'owner_org': source_dataset.owner_org,
                          'language': self.config.get('language'),
                          'extras': [{'key': 'source',
                                      'value': source_name}]
                          }
            )

        dataset_object = model.Package.get(dataset['id'])
        harvest_object.package = dataset_object
        f = NamedTemporaryFile(delete=False)

        resource = {}

        data = [json.loads(harvest_object.content)]
        f.write(json.dumps(data))
        fs = cgi.FieldStorage()
        fs.file = f
        fs.filename = 'scraper.json'

        if len(dataset['resources']) > 0:
            resource = dataset['resources'][0]
            resource['upload'] = fs
            get_action('resource_update')(context, resource)
        else:
            resource = get_action('resource_create')(
                context, {'package_id': dataset['id'],
                          'url': 'scraper.json',
                          'upload': fs,
                          'format': 'JSON'}
            )
        f.close()
        harvest_object.current = True
        harvest_object.package_id = dataset['id']
        harvest_object.save()

        return True


class ContentFetchError(Exception):
    pass
